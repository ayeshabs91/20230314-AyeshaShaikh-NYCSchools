//
//  UIView+Util.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/*
 * UIView Extension methods
 */
extension UIView {

    /// Constraints to embed child view into parent view with given insets
    ///
    /// - Parameters:
    ///   - childView: child view
    ///   - parentView: parent view
    ///   - insets: padding to add the child view, default to 0
    func embed(childView: UIView, in parentView: UIView, insets: UIEdgeInsets = .zero) {
        NSLayoutConstraint.activate([
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: insets.top),
            childView.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: insets.left),
            childView.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: -insets.right),
            parentView.bottomAnchor.constraint(equalTo: childView.bottomAnchor, constant: insets.bottom)
        ])
    }
}
