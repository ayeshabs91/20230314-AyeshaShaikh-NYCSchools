//
//  ContactHelper.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit
import MapKit

/*
 * Helper class for contact view
 */
class ContactHelper {
    
    /// Open website or phone URL
    static func open(_ url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    /// Open Maps
    static func openInMaps(latitude: String, longitude: String, name: String?) {
        let location = CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
        let placemark = MKPlacemark(coordinate: location)

        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name

        // open the Maps app with the map item
        mapItem.openInMaps(launchOptions: nil)
    }
}
