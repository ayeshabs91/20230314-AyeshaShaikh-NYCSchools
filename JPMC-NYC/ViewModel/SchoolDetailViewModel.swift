//
//  SchoolDetailViewModel.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation

/**
 View model class for detail view controller to display school details
 */
class SchoolDetailViewModel {
    
    var apiManager: APIManager
    var school: School
    var satResult: SATResult? = nil
    
    // MARK: - Init
    
    init(apiManager: APIManager, school: School) {
        self.apiManager = apiManager
        self.school = school
    }
    
    // MARK: - API Interface
    
    func getSchoolDetails(completion: @escaping (APIError?) -> ()) {
        guard let dbn = school.dbn else {
            completion(.invalidRequest)
            return
        }
        guard let url = EndPoint().urlForSATScore(for: dbn) else {
            completion(.invalidRequest)
            return
        }
        self.apiManager.fetch(from: url, of: [SATResult].self) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                completion(error)
                
            case .success(let satResult):
                guard let self = self else { return }
                self.satResult = satResult.first
                completion(nil)
            }
        }
    }
}
