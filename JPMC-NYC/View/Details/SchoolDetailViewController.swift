//
//  SchoolDetailViewController.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
  Screen to display details of a school
 */
class SchoolDetailViewController: UIViewController {
    
    private let viewModel: SchoolDetailViewModel
    private let scoreSection: SATScoreView

    let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    
    init(with viewModel: SchoolDetailViewModel) {
        self.viewModel = viewModel
        self.scoreSection = SATScoreView(frame: .zero)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupView()
        setupData()
    }
    
    /// setup view
    private func setupView() {
        view.addSubview(scrollView)
        view.embed(childView: scrollView, in: self.view,
                   insets: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
        
        scrollView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor),
            
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        // SAT section
        stackView.addArrangedSubview(scoreSection)
        
        // Overview section
        let overviewSection = OverviewView(with: viewModel.school)
        stackView.addArrangedSubview(overviewSection)
        
        // Contact Section
        let contactSection = ContactView(with: viewModel.school)
        stackView.addArrangedSubview(contactSection)
    }
    
    /// set up data
    private func setupData() {
        viewModel.getSchoolDetails { [weak self] apiError in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if let apiError = apiError {
                    self.showError(with: apiError.message)
                } else {
                    self.updateView()
                }
            }
        }
    }
    
    /// update view 
    private func updateView() {
        navigationItem.title = viewModel.school.schoolName
        if let scores = viewModel.satResult {
            scoreSection.updateView(with: scores)
        }
    }
}
