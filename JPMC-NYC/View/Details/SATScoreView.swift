//
//  SATScoreView.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
  View to display SAT scores
 */
class SATScoreView: UIView {
    
    private var readingLabel = UILabel()
    private var writingLabel = UILabel()
    private var mathLabel = UILabel()
    
    // MARK: - Init Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        isHidden = true
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 10
        layer.borderWidth = 2
        
        let title = UILabel()
        title.text = "SAT scores"
        title.font = .boldSystemFont(ofSize: 14.0)
        
        let stackView = UIStackView(arrangedSubviews: [
            title,
            format(readingLabel),
            format(mathLabel),
            format(writingLabel)
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 5.0
        addSubview(stackView)
        embed(childView: stackView, in: self,
              insets: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
    }
                
    private func format(_ labelView: UILabel) -> UILabel {
        labelView.translatesAutoresizingMaskIntoConstraints = false
        labelView.numberOfLines = 0
        labelView.textColor = .black
        labelView.font = .systemFont(ofSize: 14.0)
        
        return labelView
    }
    
    // MARK: - Public Methods
    
    func updateView(with scores: SATResult) {
        guard scores.isNotEmptyOrNil() else {
            return
        }
        isHidden = false
        readingLabel.text = String(format: "SAT Average Critical Reading score: %@", scores.sat_critical_reading_avg_score ?? "")
        mathLabel.text = String(format: "SAT Average Math score: %@", scores.sat_math_avg_score ?? "")
        writingLabel.text = String(format: "SAT Average Writing score: %@", scores.sat_writing_avg_score ?? "")
    }
}
