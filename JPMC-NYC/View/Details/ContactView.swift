//
//  ContactView.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
  View to display School contact information
 */
class ContactView: UIView {
    
    private var addressLabel = HyperlinkLabel(with: .address)
    private var phoneLabel = HyperlinkLabel(with: .phone)
    private var websiteLabel = HyperlinkLabel(with: .web)
    
    private let school: School
    
    // MARK: - Init Methods
    
    init(with school: School) {
        self.school = school
        super.init(frame: .zero)
        
        setup()
        updateView(with: school)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 10
        layer.borderWidth = 2
        
        let title = UILabel()
        title.font = .boldSystemFont(ofSize: 14.0)
        title.text = "Contact (Tap on the links to redirect)"
        
        let stackView = UIStackView(arrangedSubviews: [
            title,
            format(labelView: addressLabel),
            format(labelView: phoneLabel),
            format(labelView: websiteLabel)
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10.0
        addSubview(stackView)
        embed(childView: stackView, in: self,
              insets: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
        
        websiteLabel.onTapped = { [weak self] in
            guard let url = self?.school.getWebsiteUrl else { return }
            ContactHelper.open(url)
        }
        
        phoneLabel.onTapped = { [weak self] in
            guard let url = self?.school.getPhoneLink else { return }
            ContactHelper.open(url)
        }
        
        addressLabel.onTapped = { [weak self] in
            guard let self = self else { return }
            if let latitude = self.school.latitude, let longitude = self.school.longitude {
                ContactHelper.openInMaps(latitude: latitude, longitude: longitude, name: self.school.schoolName)
            }
        }
    }
                
    private func format(labelView: UILabel) -> UILabel {
        labelView.textColor = .black
        labelView.numberOfLines = 0
        labelView.lineBreakMode = .byWordWrapping
        labelView.font = .systemFont(ofSize: 14.0)
        
        return labelView
    }
    
    // MARK: - Public Methods
    
    func updateView(with school: School) {
        addressLabel.text = String(format: "Address: %@ ", school.getAddressWithoutCoordinates ?? "")
        phoneLabel.text = String(format: "Ph: %@", school.phoneNumber ?? "")
        websiteLabel.text = String(format: "%@", school.website ?? "")
    }
}
