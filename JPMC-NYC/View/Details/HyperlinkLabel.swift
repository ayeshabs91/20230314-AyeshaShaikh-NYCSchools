//
//  HyperlinkLabel.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

enum LinkType {
    case web
    case phone
    case address
}

/**
  Hyperlink label view with tap gesture recognizers
 */
class HyperlinkLabel: UILabel {
    var onTapped: (() -> ())? = nil
    private let linkType: LinkType
    
    override var text: String? {
        didSet {
            format()
        }
    }

    public init(with linkType: LinkType) {
        self.linkType = linkType
        super.init(frame: .zero)
        
        font = .systemFont(ofSize: 14.0)
        textColor = .lightGray
        isUserInteractionEnabled = true
        translatesAutoresizingMaskIntoConstraints = false
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(labelDidGetTapped(sender:))))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func labelDidGetTapped(sender: UILabel) {
        onTapped?()
    }
}

/**
  Extension for Hyperlink format methods
 */
extension HyperlinkLabel {
    
    func format() {
        switch linkType {
        case .web: didSetWebUrl()
        case .phone: didSetPhoneNumber()
        case .address: didSetAddress()
        }
    }
    
    func didSetWebUrl() {
        guard let text = text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        text.enumerateSubstrings(in: text.startIndex..<text.endIndex, options: .byWords) {
            (substring, substringRange, _, _) in
            let range = NSRange(substringRange, in: text)
            attributedString.addAttribute(.foregroundColor, value: UIColor.blue, range: range)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
            self.attributedText = attributedString
        }
    }
    
    func didSetAddress() {
        guard let text = text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        addIcon(UIImage(named: "location_icon")?.withTintColor(.black),
                to: attributedString)
    }
    
    func didSetPhoneNumber() {
        guard let text = text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        addIcon(UIImage(named: "phone")?.withTintColor(.black),
                to: attributedString)
    }
    
    func addIcon(_ icon: UIImage?, to string: NSMutableAttributedString) {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = icon
        
        let imageOffsetY: CGFloat = -5.0
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY,
                                        width: imageAttachment.image!.size.width,
                                        height: imageAttachment.image!.size.height)
        
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        
        let imageAttributedString = NSMutableAttributedString(string: "")
        imageAttributedString.append(attachmentString)
        imageAttributedString.append(NSAttributedString(string: "  "))
        imageAttributedString.append(string)
        self.attributedText = imageAttributedString
    }
}
