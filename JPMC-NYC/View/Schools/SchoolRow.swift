//
//  SchoolRow.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
 Cell view to display school information
 */
class SchoolRow: UITableViewCell {
    
    private let schoolNameLabel = UILabel()
    private let addressLabel = UILabel()
    private let phoneLabel = UILabel()
    private let vStack = UIStackView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        layer.cornerRadius = 10
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 2
        
        vStack.translatesAutoresizingMaskIntoConstraints = false
        vStack.spacing = 10.0
        vStack.axis = .vertical
        contentView.addSubview(vStack)
        embed(childView: vStack, in: contentView,
              insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
        schoolNameLabel.translatesAutoresizingMaskIntoConstraints = false
        schoolNameLabel.textColor = .black
        schoolNameLabel.font = .boldSystemFont(ofSize: 16.0)
        schoolNameLabel.numberOfLines = 0
        vStack.addArrangedSubview(schoolNameLabel)
        
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.textColor = .darkGray
        addressLabel.font = .systemFont(ofSize: 14.0)
        addressLabel.numberOfLines = 0
        vStack.addArrangedSubview(addressLabel)
        
        phoneLabel.translatesAutoresizingMaskIntoConstraints = false
        phoneLabel.textColor = .darkGray
        phoneLabel.font = .systemFont(ofSize: 14.0)
        vStack.addArrangedSubview(phoneLabel)
    }
    
    func configure(with school: School) {
        schoolNameLabel.text = school.schoolName
        addressLabel.text = school.getAddressWithoutCoordinates
        phoneLabel.text = school.phoneNumber
    }
}
